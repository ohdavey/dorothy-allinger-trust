/*

 */

function sendApplication(data) {
    $.ajax({
        url: "/application",
        method: "POST",
        data: data,
        dataType: "json"
    }).done(function(data, textStatus, jqXHR) {
        var msg = data.message;
        $('#form-response').removeClass().addClass('alert alert-success').html(msg);
        $('#applicationForm button').prop('disabled', true);


    }).fail(function(jqXHR, textStatus, errorThrown) {


        $('#applicationForm button').prop('disabled', false);
        var statusCode = jqXHR.status;
        if (statusCode == 400) { //Validation failure on the server side
            if (jqXHR.responseJSON) {
                var response = '';
                response = JSON.parse(jqXHR.responseJSON.message);
                var errors = Object.keys(response);
                errors.forEach(function(error) {
                    msg += '<p><b class="text-capitalize"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> ' + error + ': </b>' + response[error] + '</p>';
                });
            }
        } else {
            msg = 'Oops. There was an error processing your request. Please try again later.';
        }
        if (statusCode == 500) {
            if (jqXHR.responseJSON) {
                msg = jqXHR.responseJSON.message;
            }
        }

        $('#form-response').removeClass().addClass('alert alert-danger').html(msg);
    }).complete(function(qXHR, textStatus) {
        // Clean up.
    });
}
$(document).ready(function() {
    var $form = $("#applicationForm");
    var $formResponse = $("#form-response");
    var $email = $("#email");
    // Validate group purchase

    $form.validate({
        onkeyup: false,
        rules: {
            firstName: "required",
            lastName: "required",
            birthdate:{
                required: true,
                date: true
            },
            email: {
                required: true,
                email: true
            },
            telephone: "required",
            address: "required",
            city: "required",
            state: "required",
            zipcode: {
                required: true,
                number: true
            },
            amount: {
                required: true,
                number:true,
                range: [1000, 8000]
            },
            university: "required",
            essay1: {
                required: true,
            },
            essay2: {
                required: true,
            },
            hsname: "required",
            hscname: "required",
            hsemail: {
                required: true,
                email: true
            },
            hstelephone: "required",
            hsaddress: "required",
            agreement: "required"
        },
        messages: {

            firstName: "Your first name is required.",
            lastName: "Your last name is required.",
            birthdate:{
                required: "Your date of birth is required.",
                date: "invalid date format."
            },
            email: {
                required: "Your email is required",
                email: "This is not a valid email"
            },
            telephone: "Your telephone is required.",
            address: "Your address is required.",
            city: "Your city is required.",
            state: "Your state is required.",
            zipcode: {
                required: "Your zipcode is required.",
                number: "This is not a valid zipcode"
            },
            amount: {
                required: "The amount is required.",
                number: "This is not a valid number.",
                range: "Must be between $1000 - $8000."
            },
            university: "Your university is required.",
            essay1: {
                required: "Your simple narrative is required."
            },
            essay2: {
                required: "Please specify if yes or no"
            },
            hsname:  "Your high school name is required.",
            hscname: "Your high school counselor name is required.",
            hsemail: {
                required: "Your high school counselor email is required",
                email: "This is not a valid email"
            },
            hstelephone: "High School telephone is required.",
            hsaddress: "High School address is required.",
            agreement: "You must agree to the terms: "

        },
        highlight: function(element, errorClass, validClass) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            $(element).siblings("span").removeClass('glyphicon-ok').addClass('glyphicon-remove');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            $(element).siblings("span").removeClass('glyphicon-remove').addClass('glyphicon-ok');
        }
    });

    $form.submit(function(event) {

        $formResponse.removeClass().empty();

        var progressbar =
            '<div class="progress">' +
            '<div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">' +
            '<span class="sr-only">45% Complete</span>' +
            'Processing' +
            '</div>' +
            '</div>';

        event.preventDefault();

        if ($form.valid()) {
            $('#applicationForm button').prop('disabled', true);
            var data = $(this).serialize();
            sendApplication(data);
            $formResponse.html(progressbar).fadeIn('fast');
        }
    });
});
