<?php

require_once "vendor/autoload.php";
require_once "local_var.php";
require_once "mail.php";

//Init monolog
$log = new \Monolog\Logger('Application-Form');
$handler = new \Monolog\Handler\StreamHandler($LOCAL_VAR['LOG_FILE'], \Monolog\Logger::INFO);
$formatter = new \Monolog\Formatter\LineFormatter(null, null, false, true);
$handler->setFormatter($formatter);
$log->pushHandler($handler);

$validator = new \Valitron\Validator($_POST);

$log->addInfo(" ");
$log->addInfo("Application Initiated by: " . $_POST['firstname'] ." ". $_POST['lastname']);

$validator->rule('required', ['firstname', 'lastname', 'birthdate', 'email', 'telephone', 'address', 'city', 'state', 'zipcode',
  'amount', 'university', 'essay1', 'essay2', 'hsname', 'hscname', 'hsemail', 'hstelephone', 'hsaddress']);

try {
  $log->addInfo("Validating user information.");
  if($validator->validate()){
    //log
    $log->addInfo("Email: ". $_POST['email']);
    $log->addInfo("University: ". $_POST['university']);
    $log->addInfo("Creating a database connection.");
    //Create a database connection and insert into db
    $db = new PDO($LOCAL_VAR['DB_HOST'], $LOCAL_VAR['DB_USER'], $LOCAL_VAR['DB_PASSWORD']);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $log->addInfo("Storing information in the databse.");
    $result = $db->prepare("INSERT INTO application(firstname, lastname, birthdate, email, telephone, address, city, state, zipcode, amount, university, essay1, essay2, highschool, counselor_name, counselor_email, highschool_telephone, highschool_address, created)
      VALUES (:firstname, :lastname, :birthdate, :email, :telephone, :address, :city, :state, :zipcode, :amount, :university, :essay1, :essay2, :highschool, :counselor_name, :counselor_email, :highschool_telephone, :highschool_address, now())");
    $result->bindParam(':firstname', $_POST['firstname']);
    $result->bindParam(':lastname', $_POST['lastname']);
    $result->bindParam(':birthdate', $_POST['birthdate']);
    $result->bindParam(':email', $_POST['email']);
    $result->bindParam(':telephone', $_POST['telephone']);
    $result->bindParam(':address', $_POST['address']);
    $result->bindParam(':city', $_POST['city']);
    $result->bindParam(':state', $_POST['state']);
    $result->bindParam(':zipcode', $_POST['zipcode']);
    $result->bindParam(':amount', $_POST['amount']);
    $result->bindParam(':university', $_POST['university']);
    $result->bindParam(':essay1', $_POST['essay1']);
    $result->bindParam(':essay2', $_POST['essay2']);
    $result->bindParam(':highschool', $_POST['hsname']);
    $result->bindParam(':counselor_name', $_POST['hscname']);
    $result->bindParam(':counselor_email', $_POST['hsemail']);
    $result->bindParam(':highschool_telephone', $_POST['hstelephone']);
    $result->bindParam(':highschool_address', $_POST['hsaddress']);
    $result->execute();

    // Send email
    $log->addInfo("Sending success email.");
    Mail::sendRegistration($_POST);

        //Send success message
    http_response_code(200);
    $log->addInfo("Returning success message.");
    $return['message'] = "Thanks, Your application was submitted successfully and you should receive a confirmation email at ". $_POST['email'] . ".";
  }
  else{
    http_response_code(400);

    $log->addInfo("Error 400 - Some information is probably missing.");
    $return['message'] = 'Oops!!! Some information is missing.';
  }
}

catch (PDOException $ex) {
    //Send error message to client
  $log->addInfo($ex);
  http_response_code(500);
  $return['message'] = "There was an error storing your information, if this issue persist contact us.";
}
catch(Exception $e)  {
  $log->addInfo($e);
  http_response_code(500);
  $return['message'] = 'There was an error with your request, please try again later.';
}

header('Content-type: application/json');
echo json_encode($return);
