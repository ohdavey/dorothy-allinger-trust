<?php


require_once('vendor/autoload.php');
require_once('local_var.php');

class Mail {

  static function sendRegistration($application) {
  // get post values
  global $LOCAL_VAR;
  $website = $LOCAL_VAR['WEBSITE'];
  $site_title = $LOCAL_VAR['SITE_TITLE'];
  $brand_img= $LOCAL_VAR['BRAND_IMG'];
  $site_email= $LOCAL_VAR['EMAIL'];
  $firstname = $application['firstname'];
  $lastname = $application['lastname'];
  $birthdate = $application['birthdate'];
  $email = $application['email'];
  $telephone = $application['telephone'];
  $address = $application['address'];
  $city = $application['city'];
  $state = $application['state'];
  $zipcode = $application['zipcode'];
  $amount = $application['amount'];
  $university = $application['university'];
  $essay1 = $application['essay1'];
  $essay2 = $application['essay2'];
  $hsname = $application['hsname'];
  $hscname = $application['hscname'];
  $hsemail = $application['hsemail'];
  $hstelephone = $application['hstelephone'];
  $hsaddress = $application['hsaddress'];
  $agreement = $application['agreement'];

 $name = $firstname . ' ' . $lastname;

  $message = '
  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
  <html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

  </head>
  <body>
  <div style=" width:auto; padding:5px 20px 20px 10px;">
  <div>
  <p>Hello,</p>
  <p>An application was submitted by '.$name.' through the DorothyAllinger.org website.</p>
  <h2>Details Below</h2>

  <div class="panel panel-default">
    <div class="panel-heading"><strong>Full Name:</strong></div>
    <ul class="list-group">
        <li class="list-group-item">'.$name.'</li>
    </ul>

    <div class="panel-heading"><strong>Day of Birth:</strong></div>
    <ul class="list-group">
        <li class="list-group-item">'.$birthdate.'</li>
    </ul>

    <div class="panel-heading"><strong>Email Address:</strong></div>
    <ul class="list-group">
        <li class="list-group-item">'.$email.'</li>
    </ul>

    <div class="panel-heading"><strong>Telephone:</strong></div>
    <ul class="list-group">
        <li class="list-group-item">'.$telephone.'</li>
    </ul>

    <div class="panel-heading"><strong>Applicant Address:</strong></div>
    <ul class="list-group">
        <li class="list-group-item">'.$address.'<br/>'.$city.', '.$state.' '.$zipcode.'</li>
    </ul>

    <div class="panel-heading"><strong>Amount of scholarship requested:</strong></div>
    <ul class="list-group">
        <li class="list-group-item">$'.$amount.'.00</li>
    </ul>

    <div class="panel-heading"><strong>Which university have you received an acceptance letter from?</strong></div>
    <ul class="list-group">
        <li class="list-group-item">'.$university.'</li>
    </ul>

    <div class="panel-heading"><strong>Simple Narrative: (500 words or less)</strong></div>
    <ul class="list-group">
        <li class="list-group-item">'.$essay1.'</li>
    </ul>

    <div class="panel-heading"><strong>Will you receive other financial aid such as Pell Grant or other state of local scholarship programs? List those grants/loans/scholarships and their amounts here.</strong></div>
    <ul class="list-group">
        <li class="list-group-item">'.$essay2.'</li>
    </ul>

    <div class="panel-heading"><strong>High School Name:</strong></div>
    <ul class="list-group">
        <li class="list-group-item">'.$hsname.'</li>
    </ul>

    <div class="panel-heading"><strong>HS Counselor Name:</strong></div>
    <ul class="list-group">
        <li class="list-group-item">'.$hscname.'</li>
    </ul>

    <div class="panel-heading"><strong>Counselor Email:</strong></div>
    <ul class="list-group">
        <li class="list-group-item">'.$hsemail.'</li>
    </ul>

    <div class="panel-heading"><strong>Counselor Telephone:</strong></div>
    <ul class="list-group">
        <li class="list-group-item">'.$hstelephone.'</li>
    </ul>

    <div class="panel-heading"><strong>Counselor Address:</strong></div>
    <ul class="list-group">
        <li class="list-group-item">'.$hsaddress.'</li>
    </ul>

    <div class="panel-heading"><strong>Student acknowledges that all information provided is valid and any false information will disqualify candidate immediately.</strong></div>
    <ul class="list-group">
        <li class="list-group-item alert alert-success">'.$agreement.'</li>
    </ul>

  </div>
  <p>
  Thank You,  </p>
  <p><b><a style="text-decoration:none;" href="http://dorothyallinger.org/">DorothyAllinger.org</a></b><br />
  <b style="font-size:14px;">Applicant Notification<br />
  <a href="mailto:dorothyallingertrust@gmail.com"> dorothyallingertrust@gmail.com</a></b>
  </p>
  </div>

  </div>

  </body>
  </html>

  </body>
  </html>';

  $to = $site_email;
  $subject = "Web Message From $name";
  $headers  = 'MIME-Version: 1.0' . "\r\n";
  $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
  $headers .= 'From: '.$name.' <'.$email.'>' . "\r\n";

  mail($to,$subject,$message,$headers);


  $x_message = '
  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
  <html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

  </head>
  <body>
  <div style=" width:auto; padding:5px 20px 20px 10px;">
  <div>
  <p>Hello '.$name.',</p>
  <p>Your application for the Dorothy Allinger Schollarship was received. We will review your information and if selected we will contact you for more details. </p>
  <div class="well">
    <p>If you are selected, you will need to provide the following documents at a later date:</p>
    <ol>
      <li>Acceptance letter to university (WVU or UNM).</li>
      <li>Letter from high school counselor recommending this student and confirming eligibility for this scholarship.</li>
      <li>Additional financial affidavit</li>
    </ol>
  </div>
  Thank You,  </p>
  <p><b><a style="text-decoration:none;" href="http://dorothyallinger.org/">DorothyAllinger.org</a></b><br />
  <b style="font-size:14px;">Auto Notification<br />
  </p>

  </div>

  </div>

  </body>
  </html>

  </body>
  </html>';

  $x_to = $email;
  $x_subject = "DAllinger Application Confirmation";
  $x_headers  = 'MIME-Version: 1.0' . "\r\n";
  $x_headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
  $x_headers .= 'From: Dorothy Allinger Schollarship <dorothyallingertrust@gmail.com>' . "\r\n";

  mail($x_to,$x_subject,$x_message,$x_headers);

  }
}
